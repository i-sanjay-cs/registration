// server.js

const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = process.env.PORT || 3000; // Use the port provided by Heroku or default to 3000

// Middleware
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Serve static files (CSS, images, etc.)
app.use(express.static(__dirname + '/public'));

// Route for serving the registration page
app.get('/', (req, res) => {
  res.sendFile(__dirname + '/public/index.html');
});

// Route for handling registration form submission
app.post('/register', (req, res) => {
  // Process registration data here
  console.log(req.body);
  res.send('Registration successful!');
});

// Start the server
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
