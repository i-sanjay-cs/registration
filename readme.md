Live Link on Heroku :  https://assesworldline-b40a94f5f2c3.herokuapp.com/


```markdown
# CI/CD Pipeline Setup Guide

This repository contains a CI/CD pipeline for automating the build and deployment process of a web application using GitLab CI/CD.

## Prerequisites

Before setting up the CI/CD pipeline, ensure you have the following:

- Access to a GitLab account.
- A Heroku account for deploying the application.
- Basic knowledge of GitLab CI/CD configuration.
- Knowledge of the application's build process and dependencies.

## Steps to Set Up CI/CD Pipeline


- Create a `.gitlab-ci.yml` file in the root of your repository to define the CI/CD pipeline.
- Define stages such as `build` and `deploy`.
- Define variables required for the pipeline:
  ```yaml
  variables:
    NODE_ENV: production
    HEROKU_API_KEY: "API-key"
    HEROKU_APP_NAME: "app name"
  ```
- Implement jobs for each stage:
  - **Build job**: Set up the environment and build the application.
    ```yaml
    build:
      stage: build
      image: node:latest
      cache:
        paths:
          - node_modules/  # Cache dependencies to speed up builds
      before_script:
        - npm install  # Install dependencies
      script:
        - npm run build  # Build the application
    ```
  - **Deploy job**: Deploy the application to Heroku.
    ```yaml
    deploy:
      stage: deploy
      image: ruby:latest
      before_script:
        - apt-get update -qy
        - apt-get install -y ruby-dev
        - gem install dpl
      script:
        - dpl --provider=heroku --app=$HEROKU_APP_NAME --api-key=$HEROKU_API_KEY
      environment:
        name: production
      only:
        - main
    ```


- Ensure that the deployed application is accessible and functional.

